package com.acvetkov.game.services;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Array;

public class AssetsManager {

    private static AssetsManager instance = null;
    private static Boolean blocked = true;
    private TextureAtlas atlas;

    private String ATLAS_NAME = "sd-sprites.txt";

    public AssetsManager() {
        if (AssetsManager.blocked) {
            throw new Error("use getInstance method");
        }
        atlas = new TextureAtlas(ATLAS_NAME);
    }

    public Array<Sprite> getAnimation(String name) {
        return atlas.createSprites(name);
    }

    public static AssetsManager getInstance() {
        if (AssetsManager.instance == null) {
            AssetsManager.blocked = false;
            AssetsManager.instance = new AssetsManager();
            AssetsManager.blocked = true;
        }
        return AssetsManager.instance;
    }
}
