package com.acvetkov.game.views;

import com.acvetkov.game.services.AssetsManager;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class BaseView extends Actor {

    public AssetsManager getAssets() {
        return AssetsManager.getInstance();
    }
}
