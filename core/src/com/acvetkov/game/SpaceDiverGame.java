package com.acvetkov.game;

import com.acvetkov.game.screens.GameScreen;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;

public class SpaceDiverGame extends Game {

	@Override
	public void create() {
        Gdx.app.log("Game", "create");
        setScreen(new GameScreen());
	}

	@Override
	public void render() {
		super.render();
	}
}
