package com.acvetkov.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class GameStage extends Stage {

    public GameStage() {

    }

    @Override
    public boolean keyDown(int keyCode) {
        Gdx.app.log("game stage", String.valueOf(keyCode));
        return super.keyDown(keyCode);
    }

    public void play(Actor actor) {

    }
}
