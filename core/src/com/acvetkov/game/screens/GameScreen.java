package com.acvetkov.game.screens;

import com.acvetkov.game.actors.FlyHog;
import com.acvetkov.game.actors.SphereDefence;
import com.badlogic.gdx.Gdx;

public class GameScreen extends BaseScreen {

    public GameScreen() {
        super();
        stage.addActor(new FlyHog());
        stage.addActor(new SphereDefence());
        Gdx.app.log("stage", String.valueOf(stage.getCamera().viewportWidth));
        Gdx.app.log("stage", String.valueOf(stage.getCamera().viewportHeight));
    }

    @Override
    public void render(float delta) {
        stage.draw();
        stage.act();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void dispose() {

    }
}
