package com.acvetkov.game.screens;

import com.acvetkov.game.views.BaseView;
import com.badlogic.gdx.Screen;

public class BaseScreen extends BaseView implements Screen {

    protected GameStage stage = new GameStage();

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
