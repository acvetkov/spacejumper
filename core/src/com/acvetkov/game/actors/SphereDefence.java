package com.acvetkov.game.actors;

import com.acvetkov.game.views.BaseView;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.utils.Array;

public class SphereDefence extends BaseView {

    private Animation animation;
    private float stateTime = 0.0f;

    public SphereDefence() {
        String ANIMATION_NAME = "SPACE_POWERUP_DEF";
        Array<Sprite> sprites = super.getAssets().getAnimation(ANIMATION_NAME);
        animation = new Animation(0.03f, sprites);
        this.setSize(170.0f, 170.0f);
        this.setPosition(150.0f, 0);

        this.addListener(new InputListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                Gdx.app.log("sphere defence", String.valueOf(keycode));
                return true;
            }
        });
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        stateTime += Gdx.graphics.getDeltaTime();
        Sprite currentFrame = (Sprite) animation.getKeyFrame(stateTime, true);
        currentFrame.draw(batch);
    }
}
