package com.acvetkov.game.actors;

import com.acvetkov.game.views.BaseView;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.utils.Array;

public class FlyHog extends BaseView {

    private Animation animation;
    private float stateTime = 0.0f;
    private Sprite currentFrame;
    private float margin = Gdx.graphics.getWidth() / 2 - 64;

    public FlyHog() {
        String ANIMATION_NAME = "SPACE_FLY_HOG";
        Array<Sprite> sprites = super.getAssets().getAnimation(ANIMATION_NAME);
        animation = new Animation(0.03f, sprites);
        this.setSize(128.0f, 128.0f);
        this.setPosition(Gdx.graphics.getWidth() / 2 - 64, 0);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        stateTime += Gdx.graphics.getDeltaTime();
        currentFrame = (Sprite) animation.getKeyFrame(stateTime, true);
        currentFrame.setPosition(getX(), getY());
        currentFrame.draw(batch);
    }

    @Override
    public void act(float delta) {
        float y = getY() + 2;
        if (y > Gdx.graphics.getHeight()) {
            y = -128;
        }
        this.setY(y);
        this.setX((float) (Math.sin(y / 100) * 75) + margin);
    }
}
