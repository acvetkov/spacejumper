package com.acvetkov.game.actors;

import com.acvetkov.game.views.BaseView;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.utils.Array;

public class FlyHogExplode extends BaseView {

    private Animation animation;
    private float stateTime = 0.0f;

    public FlyHogExplode(float x, float y) {
        String ANIMATION_NAME = "SPACE_FLY_HOG";
        Array<Sprite> sprites = super.getAssets().getAnimation(ANIMATION_NAME);
        animation = new Animation(0.03f, sprites);
        this.setSize(128.0f, 128.0f);
        this.setPosition(x, y);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        stateTime += Gdx.graphics.getDeltaTime();
        Sprite currentFrame = (Sprite) animation.getKeyFrame(stateTime, true);
        currentFrame.setPosition(getX(), getY());
        currentFrame.draw(batch);
    }

    public void draw() {

    }
}
