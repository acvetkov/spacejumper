package com.acvetkov.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.acvetkov.game.SpaceDiverGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Space diver";
		config.width = 800;
		config.height = 700;
		new LwjglApplication(new SpaceDiverGame(), config);
	}
}
